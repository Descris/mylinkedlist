public class MyLinkedList<T> {
    private int size;
    private Node<T> first;
    private Node<T> last;

    public void addTail(T data) {
        Node<T> addNode = new Node<T>(data);
        Node<T> temp = last;
        if (checkFirstIsNull()) {
            first = addNode;
            last = addNode;
        } else {
            temp.setNext(addNode);
            last = addNode;
            last.setPrev(temp);
        }
        size++;
    }

    public void addHead(T head) {
        Node<T> addNode = new Node<T>(head);
        Node<T> temp = first;
        if (checkFirstIsNull()) {
            first = addNode;
            last = addNode;
        } else {
            temp.setPrev(addNode);
            first = addNode;
            first.setNext(temp);
        }
        size++;
    }

    public int getSize() {
        return size;
    }


    private T removeFirst() {
        Node<T> temp = first;
        first = temp.getNext();
        first.setPrev(null);
        size--;
        return temp.getData();


    }

    private T removeLast() {
        Node<T> temp = last;
        last = temp.getPrev();
        last.setNext(null);
        size--;
        return temp.getData();

    }


    public T remove(int index) {
        if (index == 0) {
            return removeFirst();
        } else if (index == size - 1) {
            return removeLast();
        } else {
            Node<T> temp = get(index);
            if (temp == null) {
                System.out.println("Kayfulla qayitmir");
                return null;
            } else {
                Node<T> tempPrev = temp.getPrev();
                Node<T> tempNext = temp.getNext();
                tempPrev.setNext(tempNext);
                tempNext.setPrev(tempPrev);

                size--;
                return temp.getData();
            }
        }


    }

    public Node<T> get(int index) {
        Node<T> temp = first;
        int sayac = 0;
        while (temp != null && sayac < index) {
            temp = temp.getNext();
            sayac++;
        }
        return temp;

    }

    public Node<T> getFirst() {
        return first;
    }

    public Node<T> getLast() {
        return last;
    }

    public void printList() {
        Node<T> temp = first;
        while (temp != null) {
            System.out.print(temp.getData() + " -> ");
            temp = temp.getNext();

        }
        System.out.printf("Size: %d", size);
        System.out.println();
    }

    public int indexOf(T element) {
        int count = 0;
        Node<T> temp = first;
        while (temp != null && temp.getData() != element) {
            temp = temp.getNext();
            count++;
        }
        if (count >= size) {
            System.out.println(" tapilmadi");
            return 0;
        }
        return count;
    }

    public boolean removeByValue(T element) {

        if (first.getData() == element) {
            removeFirst();
            return true;
        } else if (last.getData() == element) {
            removeLast();
            return true;
        } else {
            Node<T> temp = first;
            while (temp != null && temp.getData() != element) {
                temp = temp.getNext();

            }
            if (temp == null) {
                return false;
            } else {
                Node<T> tempPrev = temp.getPrev();
                Node<T> tempNext = temp.getNext();
                tempPrev.setNext(tempNext);
                tempNext.setPrev(tempPrev);
                size--;
                return true;
            }

        }
    }

    public boolean contains(T element) {
        int count = 0;
        Node<T> temp = first;
        while (temp != null && temp.getData() != element) {
            temp = temp.getNext();
            count++;
        }
        return count != size;
    }

    ;

    private boolean checkFirstIsNull() {
        return first == null;
    }
}
